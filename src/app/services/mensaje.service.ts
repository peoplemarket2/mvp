import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MensajeService {
  mensaje: string[] = [];

  add(mensaje: string) {
    this.mensaje = [...this.mensaje, mensaje];
  }

  clear() {
    this.mensaje = [];
  }

}
