import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicacionesRoutingModule } from './publicaciones-routing.module';
import { PublicacionesComponent } from './publicaciones.component';
import { PublicacionesContainer } from './publicaciones.container';
import { RegistrarPComponent } from './registrar-p/registrar-p.component';
import { RegistrarPContainerComponent } from './registrar-p/registrar-p.container';
import { MatCardModule} from '@angular/material/card';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatIconModule} from '@angular/material/icon';
import { MatButtonModule} from '@angular/material/button';
import { MatInputModule} from '@angular/material/input';
import { ReactiveFormsModule} from '@angular/forms';
import { FormsModule} from '@angular/forms';
import { MatStepperModule} from '@angular/material/stepper';
import { MatSidenavModule} from '@angular/material/sidenav';
import { MatTableModule} from '@angular/material/table';
import { ViewsComponent } from './views/views.component';
import { ViewsContainerComponent } from './views/views.container';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { CrearCategoriaComponent } from './crear-categoria/crear-categoria.component';
import { CrearCategoriaContainer } from './crear-categoria/crear-categoria.container';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ModalComponent } from './registrar-p/modal/modal.component';
import { MatDialogModule} from '@angular/material/dialog';
import { EditarPComponent } from './editar-p/editar-p.component';
import { HomeComponent } from './home/home.component';
@NgModule({
  declarations: [
    PublicacionesComponent,
    PublicacionesContainer,
    RegistrarPComponent,
    RegistrarPContainerComponent,
    ViewsComponent,
    ViewsContainerComponent,
    CrearCategoriaComponent,
    CrearCategoriaContainer,
    ModalComponent,
    EditarPComponent,
    HomeComponent
],
  imports: [
    CommonModule,
    PublicacionesRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatStepperModule,
    MatSidenavModule,
    MatTableModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatDialogModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [PublicacionesContainer]
})
export class PublicacionesModule { }
