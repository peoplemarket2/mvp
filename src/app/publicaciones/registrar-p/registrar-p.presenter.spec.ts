import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarPPresenter } from './registrar-p.presenter';

describe('RegistrarPPresenter', () => {
  let component: RegistrarPPresenter;
  let fixture: ComponentFixture<RegistrarPPresenter>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarPPresenter ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarPPresenter);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
