import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearCategoriaPresenter } from './crear-categoria.presenter';

describe('CrearCategoriaPresenter', () => {
  let component: CrearCategoriaPresenter;
  let fixture: ComponentFixture<CrearCategoriaPresenter>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearCategoriaPresenter ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearCategoriaPresenter);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
