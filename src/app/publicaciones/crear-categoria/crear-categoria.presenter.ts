import { Subject, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';



export class CrearCategoriaPresenter {

  private addC: Subject<FormGroup> = new Subject();
  addC$: Observable<FormGroup> = this.addC.pipe(
    debounceTime(300),
    distinctUntilChanged(),
  );

  crear(categoria: FormGroup): void{
    console.log(categoria);
    this.addC.next(categoria);
  }
}
