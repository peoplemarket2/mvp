import { Component,  ChangeDetectionStrategy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-publicaciones',
  templateUrl: './publicaciones.container.html',
})
// tslint:disable-next-line: component-class-suffix
export class PublicacionesContainer  { }
