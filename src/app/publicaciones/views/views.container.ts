import { Component,  } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { multiScan } from 'rxjs-multi-scan';
import { publicacion } from 'src/app/Model/publicacion';
import { PublicacionesService } from 'src/app/services/publicaciones.service';

@Component({
  selector: 'app-views',
  templateUrl: './views.container.html',
})
export class ViewsContainerComponent {
  private publicInha: Subject<publicacion> = new Subject();
  viewpub$: Observable<publicacion[]> = multiScan(
    this.publicacionServices.getPublic(),
    (publicaciones, cargaPublicacion) => [...publicaciones, ...cargaPublicacion],
    this.publicInha,
    (publicaciones, publicacions) => publicaciones.filter(p => p !== publicacions ),
    []);

  constructor(private publicacionServices: PublicacionesService) {}

  inhabilitar(pub: publicacion): void {
    if (pub.estado === 'V'){
      pub.estado = 'D';
    }else{
      pub.estado = 'V';
    }
    console.log(pub);
    this.publicacionServices.inhabilitar(pub)
    .subscribe();
  }
}
