import {  Observable,  Subject} from 'rxjs';


export class ViewsPresenter {
  private inha: Subject < string > = new Subject();
  inha$: Observable < string > = this.inha.asObservable();

  public inhaPublic(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.inha.next(name);
  }
}
