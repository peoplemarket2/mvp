import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {


  public UserGroup: FormGroup;

  private url="http://localhost/proyecto/getdata.php";

  public dataRate: any=null;


  constructor(private UserBuilder: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.getData();
    this.buildForm();
  }

  private buildForm(){
    this.UserGroup=this.UserBuilder.group({
      nombre:['', [Validators.required, Validators.minLength(20), Validators.maxLength(30)]],
      apellido:['',  [Validators.required, Validators.minLength(20), Validators.maxLength(30)]],
      documento:['', [Validators.required, Validators.minLength(8)]],
      sexo:['',[Validators.required]],
      direccion:['',[Validators.required]],
      celular:['', [Validators.required, Validators.maxLength(10)]],
      correo:['', [Validators.required, this.validarCorreo]],
      usuario: ['', Validators.required],
      contraseña:['', [Validators.required, Validators.minLength(8)] ],
    })
  }

  public registrar(){
    const usuario=this.UserGroup;
    console.log(usuario);
  }

  private validarCorreo(control: AbstractControl){
    const correo= control.value;
    let error=null;

    if(!correo.includes('@')){
      error={...error,arroba: 'Necesita ingresar el simbolo @'}
    }
    if(!correo.includes('.')){
      error={...error, punto:'Necesita ingresar el caracter "."'}

    }
    return error;
  }

  public mostrarError(controlName: string): string{
    let error= '';
    const control=this.UserGroup.get(controlName);

    if(control.touched && control.errors != null ){
      error=JSON.stringify(control.errors);

    }
    return error;

  }

  private getData(){
    const urlApi= this.url;
    this.httpClient.get(urlApi)
      .subscribe(apiData => (this.dataRate = apiData));
  }

}
